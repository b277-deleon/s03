INSERT INTO users (email, password, datetime_created) 
VALUES ("johnsmith@gmail.com", "passwordA", CAST('2021-01-01 01:00:00' AS DATETIME)),
("juandelacruz@gmail.com", "passwordB", CAST('2021-01-01 02:00:00' AS DATETIME)),
("janesmith@gmail.com", "passwordC", CAST('2021-01-01 03:00:00' AS DATETIME)),
("mariadelacruz@gmail.com", "passwordD", CAST('2021-01-01 04:00:00' AS DATETIME)),
("johndoe@gmail.com", "passwordE", CAST('2021-01-01 05:00:00' AS DATETIME));

INSERT INTO posts (author_id, title, content, datetime_posted)
VALUES (1, "First Code", "Hello World!", CAST('2021-01-02 01:00:00' AS DATETIME)),
(1, "Second Code", "Hello Earth!", CAST('2021-01-02 02:00:00' AS DATETIME)),
(2, "Third Code", "Welcome to Mars!", CAST('2021-01-02 03:00:00' AS DATETIME)),
(4, "Fourth Code", "Bye bye solar system!", CAST('2021-01-02 04:00:00' AS DATETIME));

SELECT * FROM posts WHERE author_id = 1;

SELECT email, datetime_created FROM users;

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

DELETE FROM users WHERE email = "johndoe@gmail.com";