-- [SECTION] INSERTING Records/Row

-- Syntax: INSERT INTO <table_name> (columns) VALUES (values);

INSERT INTO artists (name) VALUES ("Rivermaya");

INSERT INTO artists (name) VALUES ("Black Pink");

INSERT INTO artists (name) VALUES ("Taylor Swift");

INSERT INTO artists (name) VALUES ("New Jeans");

INSERT INTO artists (name) VALUES ("Bamboo");

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-01-01", 1);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born Pink", "2022-09-16", 2);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("The Album", "2022-10-02", 2);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Midnights", "2022-10-21", 3);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("New Jeans", "2022-08-01", 4);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Tomorrow Becomes Yesterday", "2008-09-28", 5);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-11-11", 3),
("Kill This Love", "2019-01-01", 2);

-- insert songs

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Snow on The Beach", 416, "Pop", 4), ("Anti-Hero", 320, "Pop", 4);

INSERT INTO songs (song_name, length, album_id) VALUES ("Bejeweled", 314, 4);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Masaya", 450, "Jazz", 9), ("Mr. Clay", 357, "Pop", 9), ("Noypi", 428, "OPM", 9);

-- add 2 songs per album

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata", 443, "OPM", 1), ("Himala", 408, "OPM", 1), ("Pink Venom", 306, "KPOP", 2), ("Tally", 304, "KPOP", 2), ("How You Like That", 301, "KPOP", 3), ("Ice Cream", 256, "KPOP", 3), ("Maroon", 338, "Pop", 4), ("Mastermind", 311, "Pop", 4), ("Hurt", 258, "KPOP", 5), ("Hype Boy", 300, "KPOP", 5), ("Kalayaan", 338, "OPM", 6), ("Muli", 523, "OPM", 6), ("Fifteen", 454, "Country pop", 7), ("The Way I Loved You", 403, "Country pop", 7), ("Don't Know What to Do", 322, "KPOP", 8), ("Kick It", 312, "KPOP", 8), ("Hudas", 305, "OPM", 9), ("Light Years", 228, "Rock", 9);

-- [SECTION] READ data from our database
-- SYNTAX:
    -- SELECT <column_name> from <table_name>;

SELECT * FROM songs;

-- Specify columns that will be shown

SELECT song_name from songs;

SELECT song_name, genre from songs;

SELECT * from songs WHERE genre = "OPM";

SELECT * from albums WHERE artist_id = 3;

--  AND and OR keywords for multiple expressions

SELECT song_name, length from songs WHERE genre = "OPM" AND length > 400;

SELECT song_name, length, genre from songs WHERE genre = "OPM" OR genre = "Jazz";

-- [SECTION] UPDATING RECORDS/DATA
-- UPDATE <table_name> SET <column_name> = <value_tobe> WHERE <condition>

UPDATE songs SET length = 428 WHERE song_name = "Masaya";

UPDATE songs SET genre = "Original Pinoy Music" WHERE genre = "OPM";

UPDATE songs SET genre = "Pop" WHERE genre is NULL;

-- [SECTION] DELETE RECORDS/DATA
-- DELETE FROM <table_name> WHERE <condition>
-- Delete all OPM songs that are more then 4 minutes

DELETE FROM songs WHERE length > 400 AND genre = "Original Pinoy Music";

-- Mini activity
    -- Delete Pop songs that are greater than 5 minutes

DELETE FROM songs WHERE length > 500 AND genre = "Pop";
